package com.t1.yd.tm.api.service;


import com.t1.yd.tm.api.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDataEndpoint getDataEndpointClient();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    IUserEndpoint getUserEndpointClient();

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

}
