package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.marker.IntegrationCategory;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_LOGIN;
import static com.t1.yd.tm.constant.EndpointTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Nullable
    private String token;
    @Nullable
    private Project testProject;

    @Nullable
    private Project createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setToken(token);
        request.setName("PROJECT1");
        request.setDescription("Description");
        @Nullable final Project projectCreated = projectEndpoint.createProject(request).getProject();
        return projectCreated;
    }

    @Nullable
    private Project findProjectById(final String Id) {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest();
        request.setToken(token);
        request.setId(Id);
        return projectEndpoint.showProjectById(request).getProject();
    }

    @Nullable
    private Project findProjectByIndex(final Integer Index) {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest();
        request.setToken(token);
        request.setIndex(Index);
        return projectEndpoint.showProjectByIndex(request).getProject();
    }

    @Before
    public void before() {
        token = login(ADMIN_LOGIN, ADMIN_PASSWORD);
        saveBackup(token);
        testProject = createProject();
    }

    @After
    public void after() {
        loadBackup(token);
        logout(token);
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setToken(token);
        request.setId(testProject.getId());
        request.setStatus(Status.COMPLETED.toString());
        projectEndpoint.changeProjectStatusById(request);
        @Nullable Project projectFound = findProjectById(testProject.getId());
        Assert.assertEquals(projectFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Status statusBefore = findProjectByIndex(0).getStatus();
        @NotNull final Status statusToSet = statusBefore == Status.COMPLETED ? Status.NOT_STARTED : Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setStatus(statusToSet.toString());
        projectEndpoint.changeProjectStatusByIndex(request);
        @Nullable Project projectFound = findProjectByIndex(0);
        Assert.assertEquals(projectFound.getStatus(), statusToSet);
    }

    @Test
    public void clearProjects() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        request.setToken(token);
        projectEndpoint.clearProjects(request);
        @NotNull final ProjectNotFoundException projectNotFoundException = new ProjectNotFoundException();
        thrown.expectMessage(projectNotFoundException.getMessage());
        Assert.assertNull(findProjectById(testProject.getId()));
    }

    @Test
    public void completeProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest changeStatusRequest = new ProjectChangeStatusByIdRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setId(testProject.getId());
        changeStatusRequest.setStatus(Status.IN_PROGRESS.toString());
        projectEndpoint.changeProjectStatusById(changeStatusRequest);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest();
        request.setToken(token);
        request.setId(testProject.getId());
        projectEndpoint.completeProjectById(request);
        @Nullable final Project projectFound = findProjectById(testProject.getId());
        Assert.assertEquals(projectFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void projectCompleteByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest changeStatusRequest = new ProjectChangeStatusByIndexRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setIndex(0);
        changeStatusRequest.setStatus(Status.IN_PROGRESS.toString());
        projectEndpoint.changeProjectStatusByIndex(changeStatusRequest);
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.completeProjectByIndex(request);
        @Nullable final Project projectFound = findProjectByIndex(0);
        Assert.assertEquals(projectFound.getStatus(), Status.COMPLETED);
    }

    @Test
    public void listProjects() {
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setToken(token);
        request.setSort(Sort.BY_NAME.toString());
        @Nullable final List<Project> projectList = projectEndpoint.listProjects(request).getProjects();
        Assert.assertTrue(projectList.size() >= 1);
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest();
        request.setToken(token);
        request.setId(testProject.getId());
        projectEndpoint.removeProjectById(request);
        @NotNull final ProjectNotFoundException projectNotFoundException = new ProjectNotFoundException();
        thrown.expectMessage(projectNotFoundException.getMessage());
        Assert.assertNull(findProjectById(testProject.getId()));
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull final Project projectFound = findProjectByIndex(0);
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.removeProjectByIndex(request);
        Assert.assertNotEquals(projectFound, findProjectByIndex(0));
    }

    @Test
    public void startProjectById() {
        @NotNull final ProjectChangeStatusByIdRequest changeStatusRequest = new ProjectChangeStatusByIdRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setId(testProject.getId());
        changeStatusRequest.setStatus(Status.NOT_STARTED.toString());
        projectEndpoint.changeProjectStatusById(changeStatusRequest);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setToken(token);
        request.setId(testProject.getId());
        projectEndpoint.startProjectById(request);
        @Nullable final Project projectFound = findProjectById(testProject.getId());
        Assert.assertEquals(projectFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startProjectByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest changeStatusRequest = new ProjectChangeStatusByIndexRequest();
        changeStatusRequest.setToken(token);
        changeStatusRequest.setIndex(0);
        changeStatusRequest.setStatus(Status.NOT_STARTED.toString());
        projectEndpoint.changeProjectStatusByIndex(changeStatusRequest);
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        projectEndpoint.startProjectByIndex(request);
        @Nullable final Project projectFound = findProjectByIndex(0);
        Assert.assertEquals(projectFound.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void updateProjectById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setToken(token);
        request.setId(testProject.getId());
        request.setName(testProject.getName() + "1");
        request.setDescription(testProject.getDescription() + "1");
        projectEndpoint.updateProjectById(request);
        @Nullable final Project projectFound = findProjectById(testProject.getId());
        Assert.assertEquals(projectFound.getName(), testProject.getName() + "1");
        Assert.assertEquals(projectFound.getDescription(), testProject.getDescription() + "1");
    }

    @Test
    public void updateProjectByIndex() {
        @Nullable final Project projectToChange = findProjectByIndex(0);
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setToken(token);
        request.setIndex(0);
        request.setName(projectToChange.getName() + "1");
        request.setDescription(projectToChange.getDescription() + "1");
        projectEndpoint.updateProjectByIndex(request);
        @Nullable final Project projectFound = findProjectById(projectToChange.getId());
        Assert.assertEquals(projectFound.getName(), projectToChange.getName() + "1");
        Assert.assertEquals(projectFound.getDescription(), projectToChange.getDescription() + "1");
    }

}

