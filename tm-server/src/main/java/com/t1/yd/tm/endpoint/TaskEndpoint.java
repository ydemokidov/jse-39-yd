package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.task.*;
import com.t1.yd.tm.dto.response.task.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Session;
import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public @NotNull TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskBindToProjectRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        getServiceLocator().getProjectTaskService().bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUnbindFromProjectRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String projectId = request.getProjectId();
        getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskChangeStatusByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.valueOf(status));
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskChangeStatusByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.valueOf(status));
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskClearResponse clearTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskClearRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        getServiceLocator().getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskCompleteByIdResponse completeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCompleteByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskCompleteByIndexResponse completeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCompleteByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskCreateRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskListByProjectIdResponse listTasksByProjectId(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListByProjectIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @Override
    @WebMethod
    public @NotNull TaskListResponse listTasks(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskListRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String sort = request.getSort();
        @NotNull final List<Task> tasks = getServiceLocator().getTaskService().findAll(userId, Sort.valueOf(sort));
        return new TaskListResponse(tasks);
    }

    @Override
    @WebMethod
    public @NotNull TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskRemoveByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().removeTaskById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskRemoveByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().removeTaskByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskShowByIdResponse showTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskShowByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @Nullable final Task task = getServiceLocator().getTaskService().findTaskById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskShowByIndexResponse showTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskShowByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @Nullable final Task task = getServiceLocator().getTaskService().findTaskByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskStartByIdResponse startTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskStartByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskStartByIndexResponse startTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskStartByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Task task = getServiceLocator().getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUpdateByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String taskId = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final TaskUpdateByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getServiceLocator().getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}