package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IProjectEndpoint;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.project.*;
import com.t1.yd.tm.dto.response.project.*;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Session;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectChangeStatusByIdRequest request) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String status = request.getStatus();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.valueOf(status));
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectChangeStatusByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String status = request.getStatus();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.valueOf(status));
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @WebMethod
    public ProjectClearResponse clearProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectClearRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        getServiceLocator().getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCompleteByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCompleteByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @Override
    @WebMethod
    public ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectCreateRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @WebMethod
    public ProjectListResponse listProjects(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectListRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Sort sort = Sort.valueOf(request.getSort());
        @NotNull final List<Project> projects = getServiceLocator().getProjectService().findAll(userId, sort);
        ProjectListResponse response = new ProjectListResponse();
        response.setProjects(projects);
        return response;
    }

    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectRemoveByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectRemoveByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectShowByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().findProjectById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectShowByIndexResponse showProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectShowByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().findProjectByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectStartByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectStartByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final Project project = getServiceLocator().getProjectService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new ProjectStartByIndexResponse(project);
    }

    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectUpdateByIdRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final String id = request.getId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ProjectUpdateByIndexRequest request) {
        @NotNull final Session session = check(request);
        @NotNull final String userId = session.getUserId();
        @NotNull final Integer index = request.getIndex();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Project project = getServiceLocator().getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}