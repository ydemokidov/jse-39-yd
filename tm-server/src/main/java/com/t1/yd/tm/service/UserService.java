package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ISaltProvider;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final ISaltProvider saltProvider;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final ISaltProvider saltProvider) {
        super(connectionService);
        this.saltProvider = saltProvider;
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ITaskRepository.class);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IUserRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();

        removeById(user.getId());
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByEmail(@NotNull final String email) {
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();

        removeById(user.getId());
        return user;
    }

    @NotNull
    @SneakyThrows
    public User remove(@NotNull final User userToRemove) {
        SqlSession sqlSession = connectionService.openSession();

        @Nullable final User user = findOneById(userToRemove.getId());
        if (user == null) throw new UserNotFoundException();

        @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
        taskRepository.clearWithUserId(userToRemove.getId());

        @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
        projectRepository.clearWithUserId(userToRemove.getId());

        removeByLogin(userToRemove.getLogin());

        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new IdEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(@NotNull final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email, @Nullable Role role) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        if (isLoginExist(login)) throw new IsLoginExistException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password, saltProvider)));
        user.setEmail(email);
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        try (@NotNull final SqlSession sqlSession = connectionService.openSession()) {
            @NotNull final IUserRepository repository = getRepository(sqlSession);
            user = repository.findByLogin(login);
        }
        return user;

    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user;
        try (@NotNull final SqlSession sqlSession = connectionService.openSession()) {
            @NotNull final IUserRepository repository = getRepository(sqlSession);
            user = repository.findByEmail(email);
        }
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void lockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    @SneakyThrows
    public void unlockByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public @NotNull List<User> findAll(@NotNull String sort) {
        return null;
    }
}