package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Task;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Task findTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task findTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = removeById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task removeTaskByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = removeByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task(userId, name, description);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setName(name);
        task.setDescription(description);
        update(task);

        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();

        task.setName(name);
        task.setDescription(description);
        update(task);

        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();

        task.setStatus(status);
        update(task);

        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();

        task.setStatus(status);
        update(task);

        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.openSession()) {
            @NotNull final ITaskRepository repository = getRepository(sqlSession);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ITaskRepository.class);
    }

}