package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.marker.UnitCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private IProjectService service;

    @Before
    public void initRepository() {
        service = new ProjectService(connectionService);
    }

    @Test
    public void add() {
        service.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), service.findOneById(USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), service.findOneById(USER1_PROJECT1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_PROJECTS);
        Assert.assertEquals(ALL_PROJECTS.size(), service.getSize());
        Assert.assertEquals(USER1_PROJECT2.getId(), service.findOneById(USER1_PROJECT2.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_PROJECTS);
        service.removeById(USER1_PROJECT2.getId());
        Assert.assertEquals(ALL_PROJECTS.size() - 1, service.getSize());
        Assert.assertNull(service.findOneById(USER1_PROJECT2.getId()));
    }


    @Test
    public void clear() {
        service.add(ALL_PROJECTS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.add(USER1.getId(), USER1_PROJECT2);
        service.add(ADMIN.getId(), ADMIN_PROJECT1);
        Assert.assertEquals(USER1_PROJECTS.size(), service.findAll(USER1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.add(USER1.getId(), USER1_PROJECT2);
        service.add(ADMIN.getId(), ADMIN_PROJECT1);

        Assert.assertEquals(USER1_PROJECT1.getId(), service.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        service.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), service.findOneByIndex(USER1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        service.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertNull(service.findOneByIndex(ADMIN.getId(), 1));
    }

    @Test
    public void existsById() {
        service.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(service.existsById(ADMIN.getId(), USER1_PROJECT2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.updateById(USER1.getId(), USER1_PROJECT1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertEquals(PROJECT_NAME, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.updateByIndex(USER1.getId(), 1, PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertEquals(PROJECT_NAME, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.changeStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        service.add(USER1.getId(), USER1_PROJECT1);
        service.changeStatusByIndex(USER1.getId(), 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findProjectById(USER1.getId(), USER1_PROJECT1.getId()).getStatus());
    }

    @After
    public void clearData() {
        service.clear();
    }

}
