package com.t1.yd.tm.dto.request.user;

import com.t1.yd.tm.dto.request.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLockRequest extends AbstractUserRequest {

    private String login;

}
