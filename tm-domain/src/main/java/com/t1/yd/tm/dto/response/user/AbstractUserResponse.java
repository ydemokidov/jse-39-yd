package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import com.t1.yd.tm.model.User;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@Nullable Throwable throwable) {
        super(throwable);
    }

    public AbstractUserResponse() {
    }

}
