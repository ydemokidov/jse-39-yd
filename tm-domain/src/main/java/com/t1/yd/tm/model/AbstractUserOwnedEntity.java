package com.t1.yd.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserOwnedEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    public AbstractUserOwnedEntity(@Nullable String userId) {
        this.userId = userId;
    }

}
