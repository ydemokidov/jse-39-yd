package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.system.ServerAboutRequest;
import com.t1.yd.tm.dto.request.system.ServerVersionRequest;
import com.t1.yd.tm.dto.response.system.ServerAboutResponse;
import com.t1.yd.tm.dto.response.system.ServerVersionResponse;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull IConnectionProvider connectionProvider) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerAboutRequest request);

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerVersionRequest request);

}
