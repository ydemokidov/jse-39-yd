package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import com.t1.yd.tm.model.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractTaskResponse extends AbstractResultResponse {

    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

    public AbstractTaskResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
