package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

}
