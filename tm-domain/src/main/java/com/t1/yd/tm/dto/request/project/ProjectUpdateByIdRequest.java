package com.t1.yd.tm.dto.request.project;

import com.t1.yd.tm.dto.request.AbstractUserRequestById;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectUpdateByIdRequest extends AbstractUserRequestById {

    private String name;

    private String description;

}
